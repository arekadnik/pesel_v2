package zprogramowanie_poziom_1.w3;

public class Palindrome {
    public static void main(String[] args) {


        System.out.println(isPalindorme("kobyla ma maly bok"));

    }

    public static boolean isPalindorme(String stringPalindrome) {


        stringPalindrome = stringPalindrome.toLowerCase();
        int leftSide = 0;
        int rightSide = stringPalindrome.length() - 1;

        while (leftSide <= rightSide) {

            char getLeftLetter = stringPalindrome.charAt(leftSide);
            char getRightLetter = stringPalindrome.charAt(rightSide);

            if (!(getLeftLetter >= 'a' && getLeftLetter <= 'z')) {
                leftSide++;
            } else if (!(getRightLetter >= 'a' && getRightLetter <= 'z')) {
                rightSide--;
            } else if (getRightLetter == getLeftLetter) {
                leftSide++;
                rightSide--;
            } else
                return false;
        }
        return true;
    }
}


